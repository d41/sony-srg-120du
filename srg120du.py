#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import configparser

from PyQt5 import QtWidgets

from modules.camera import Camera

config = configparser.ConfigParser()


def send_serial(port, baudrate, testmode=False):
    print("Prepare to open port {} baudrate {}".format(port, baudrate))

    if testmode:
        def send(data):
            print(repr(data))

    else:
        import serial
        sport = serial.serial_for_url(port, baudrate=baudrate, timeout=3)

        def send(data):
            sport.write(data)
            print(repr(data))

    return send


if __name__ == "__main__":
    config.read("config.ini")
    config.sections()
    port = config["Settings"].get("port", "/dev/ttyUSB0")
    baudrate = config["Settings"].getint("baudrate", 9600)
    testmode = config["Settings"].getboolean("testmode", False)

    try:
        send = send_serial(port, baudrate, testmode)
    except Exception as a:
        print("Failed to open port {} with baudrate {}".format(port, baudrate))

    app = QtWidgets.QApplication(sys.argv)
    win = Camera(send)
    win.show()
    sys.exit(app.exec_())

"""\
# VISCA over Ethernet (correct Sony version) require additional packaging of commands
# Other devices (like VHD) not requires this and work with plain VISCA sent to socket

# send to ethernet socket
import socket
host = '192.168.5.163'
port = 1259
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
    s.sendto(data, (host, port))
"""
