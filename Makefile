
all: srg120du_rc.py modules/camera_ui.py

modules/camera_ui.py: ui/camera.ui
	python3 -m PyQt5.uic.pyuic -o $@ $<


srg120du_rc.py: srg120du.qrc ui/icon.svg ui/scheme.png
	pyrcc5 -o srg120du_rc.py srg120du.qrc

