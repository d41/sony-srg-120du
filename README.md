# Simple control of Sony SRG-120DU camera

## About

Simple application to control SRG-120DU camera.

This is a USB-connected webcam with serial port driver inside.
Require to set dip switches on camera. #1 - LAN/VISCA #2 - USB ON/OFF #3 - Baud 9.6k/38.4k
Can be controlled via Visca ports and Visca over Ethernet, but here is via integrated Visca over USB to Serial.


## Deployment

After developing the only files needed to run:

	modules/*.py
	srg120du.py
	srg120du_rc.pu
	config.ini


## TODO

1. Sony variant of VISCA over Ethernet
2. Translation
3. Different types of connection in `config.ini`


BSD-3-Clause