#!/bin/env/python3
# -*- coding: utf-8 -*-

import struct


def __camera(number=1):
    if number not in range(1, 8):
        number = 0x01
    return 0x80 + number


def power(power=0, camera=1):
    """ Create camera enable/disable command

    >>> power(0)
    b'\\x81\\x01\\x04\\x00\\x03\\xff'

    >>> power(1, 3)
    b'\\x83\\x01\\x04\\x00\\x02\\xff'
    """

    section = 0x04  # main section
    subsection = 0x00  # power control
    action = (0x03, 0x02)[power]
    command = [__camera(camera), 0x01, section, subsection, action, 0xff]
    return struct.pack(b"@BBBBBB", *command)


zoom_stop = 0
zoom_tele = 1
zoom_wide = 2


def zoom(direction=zoom_stop, camera=1):
    """ Create camera zoom command

    >>> zoom(zoom_stop)
    b'\\x81\\x01\\x04\\x07\\x00\\xff'

    >>> zoom(zoom_tele, 3)
    b'\\x83\\x01\\x04\\x07\\x02\\xff'

    >>> zoom(zoom_wide, 2)
    b'\\x82\\x01\\x04\\x07\\x03\\xff'
    """
    section = 0x04  # main section
    subsection = 0x07  # zoom control
    action = (0x00, 0x02, 0x03)[direction]
    command = [__camera(camera), 0x01, section, subsection, action, 0xff]
    return struct.pack(b"@BBBBBB", *command)


pt_stop = 0
pt_up = 1
pt_down = 2
pt_left = 4
pt_right = 8


def pan_tilt(direction=pt_stop, camera=1):
    """ Create camera pan/tilt command

    >>> pan_tilt()
    b'\\x81\\x01\\x06\\x01\\x0c\\n\\x03\\x03\\xff'

    >>> pan_tilt(pt_left, 3)
    b'\\x83\\x01\\x06\\x01\\x0c\\n\\x01\\x03\\xff'

    >>> pan_tilt(pt_right+pt_up, 2)
    b'\\x82\\x01\\x06\\x01\\x0c\\n\\x02\\x01\\xff'

    >>> pan_tilt(pt_left+pt_right, 5)
    b'\\x85\\x01\\x06\\x01\\x0c\\n\\x03\\x03\\xff'
    """
    section = 0x06  # ptz section
    subsection = 0x01  # direction control
    horizontal_speed = 0x0c  # 0x01(min) - 0x18(max)
    vertical_speed = 0x0a  # 0x01(min) - 0x14(max) \x0a=\n

    horizontal = (direction & (pt_left + pt_right)) >> 2
    horizontal = 0x03 if horizontal == 0 else horizontal

    vertical = direction & (pt_up + pt_down)
    vertical = 0x03 if vertical == 0 else vertical

    command = [__camera(camera), 0x01, section, subsection, horizontal_speed, vertical_speed, horizontal, vertical,
               0xff]
    return struct.pack(b"@BBBBBBBBB", *command)


def pan_tilt_home(camera=1):
    """ Create camera command for home position of pan/tilt

    >>> pan_tilt_home(2)
    b'\\x82\\x01\\x06\\x04\\xff'
    """
    section = 0x06  # ptz section
    subsection = 0x04  # go home
    command = [__camera(camera), 0x01, section, subsection, 0xff]
    return struct.pack(b"@BBBBB", *command)


def preset_delete(number, camera=1):
    """ Reset camera memory slot

    >>> preset_delete(4)
    b'\\x81\\x01\\x04?\\x00\\x04\\xff'
    """
    if number>=0 or number<=15:
        section = 0x04  # camera control
        subsection = 0x3f  # memory
        command = [__camera(camera), 0x01, section, subsection, 0x00, number, 0xff]
        return struct.pack(b"@BBBBBBB", *command)
    return b""        


def preset_save(number, camera=1):
    """ Set camera memory slot

    >>> preset_save(4)
    b'\\x81\\x01\\x04?\\x01\\x04\\xff'
    """
    if number>=0 or number<=15:
        section = 0x04  # camera control
        subsection = 0x3f  # memory
        command = [__camera(camera), 0x01, section, subsection, 0x01, number, 0xff]
        return struct.pack(b"@BBBBBBB", *command)
    return b""        


def preset_load(number, camera=1):
    """ Recall camera memory slot

    >>> preset_load(10, camera=2)
    b'\\x82\\x01\\x04?\\x02\\n\\xff'
    """
    if number>=0 or number<=15:
        section = 0x04  # camera control
        subsection = 0x3f  # memory
        command = [__camera(camera), 0x01, section, subsection, 0x02, number, 0xff]
        return struct.pack(b"@BBBBBBB", *command)
    return b""


if "__main__" == __name__:
    import doctest

    doctest.testmod(verbose=False)



