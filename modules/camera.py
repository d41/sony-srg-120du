# -*- coding: utf-8 -*-


from PyQt5 import QtWidgets, QtCore, QtGui

from modules.camera_ui import Ui_Camera
from modules import auxilary as log
from modules import visca


class Camera(QtWidgets.QWidget):
    def __init__(self, send):
        super().__init__()
        self.ui = Ui_Camera()
        self.ui.setupUi(self)
        self.send = send

    # home
    @QtCore.pyqtSlot()
    def on_buttonHome_clicked(self):
        data = visca.pan_tilt_home(0)
        self.send(data)

    # pan section
    @QtCore.pyqtSlot()
    def on_buttonLeft_pressed(self):
        data = visca.pan_tilt(visca.pt_left)
        self.send(data)

    @QtCore.pyqtSlot()
    def on_buttonLeft_released(self):
        data = visca.pan_tilt(visca.pt_stop)
        self.send(data)

    @QtCore.pyqtSlot()
    def on_buttonRight_pressed(self):
        data = visca.pan_tilt(visca.pt_right)
        self.send(data)

    @QtCore.pyqtSlot()
    def on_buttonRight_released(self):
        data = visca.pan_tilt(visca.pt_stop)
        self.send(data)

    # tilt section
    @QtCore.pyqtSlot()
    def on_buttonUp_pressed(self):
        data = visca.pan_tilt(visca.pt_up)
        self.send(data)

    @QtCore.pyqtSlot()
    def on_buttonUp_released(self):
        data = visca.pan_tilt(visca.pt_stop)
        self.send(data)

    @QtCore.pyqtSlot()
    def on_buttonDown_pressed(self):
        data = visca.pan_tilt(visca.pt_down)
        self.send(data)

    @QtCore.pyqtSlot()
    def on_buttonDown_released(self):
        data = visca.pan_tilt(visca.pt_stop)
        self.send(data)

    # zoom section
    @QtCore.pyqtSlot()
    def on_buttonMinus_pressed(self):
        data = visca.zoom(visca.zoom_wide)
        self.send(data)

    @QtCore.pyqtSlot()
    def on_buttonMinus_released(self):
        data = visca.zoom(visca.zoom_stop)
        self.send(data)

    @QtCore.pyqtSlot()
    def on_buttonPlus_pressed(self):
        data = visca.zoom(visca.zoom_tele)
        self.send(data)

    @QtCore.pyqtSlot()
    def on_buttonPlus_released(self):
        data = visca.zoom(visca.zoom_stop)
        self.send(data)

    # presets
    @QtCore.pyqtSlot()
    def on_buttonPreset1_clicked(self):
        if not self.ui.modeSave.isChecked():
            log.stamp("load")
            data = visca.preset_load(1)
            self.send(data)
        else:
            log.stamp("save")
            data = visca.preset_save(1)
            self.send(data)

    @QtCore.pyqtSlot()
    def on_buttonPreset2_clicked(self):
        if not self.ui.modeSave.isChecked():
            log.stamp("load")
            data = visca.preset_load(2)
            self.send(data)
        else:
            log.stamp("save")
            data = visca.preset_save(2)
            self.send(data)

    @QtCore.pyqtSlot()
    def on_buttonPreset3_clicked(self):
        if not self.ui.modeSave.isChecked():
            log.stamp("load")
            data = visca.preset_load(3)
            self.send(data)
        else:
            log.stamp("save")
            data = visca.preset_save(3)
            self.send(data)

    @QtCore.pyqtSlot()
    def on_buttonPreset4_clicked(self):
        if not self.ui.modeSave.isChecked():
            log.stamp("load")
            data = visca.preset_load(4)
            self.send(data)
        else:
            log.stamp("save")
            data = visca.preset_save(4)
            self.send(data)


"""\
    # power section
    @QtCore.pyqtSlot()
    def on_pb_on_clicked(self):
        data = visca.power(1)
        send(data)

    @QtCore.pyqtSlot()
    def on_pb_off_clicked(self):
        data = visca.power(0)
        send(data)
"""
