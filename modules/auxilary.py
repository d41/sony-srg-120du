# -*- coding: utf-8 -*-

import logging

logging.getLogger().setLevel(logging.DEBUG)


def before(prefix):
    def func(function):
        def wrapper(*args, **kwargs):
            logging.debug("{} [".format(prefix))
            function(*args, **kwargs)

        return wrapper

    return func


def after(suffix):
    def func(function):
        def wrapper(*args, **kwargs):
            function(*args, **kwargs)
            logging.debug("] {}".format(suffix))

        return wrapper

    return func


def brace(name):
    def func(function):
        def wrapper(*args, **kwargs):
            print("args={}, kwargs={}".format(args, kwargs))
            logging.debug("{} [".format(name))
            function(*args, **kwargs)
            logging.debug("] {}".format(name))

        return wrapper

    return func


def stamp(text):
    logging.debug(text)
